package com.company.Parser;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.TimeUnit;

public class Main {
    public static volatile boolean isStop = false;

    public static void main(String[] args) {
        // write your code here

        //IT WORKS ONLY WITH UNF-8 ENCODED FILES
        String path1 = "/root/IdeaProjects/Parser/TextFiles/Text1";
        String path2 = "/root/IdeaProjects/Parser/TextFiles/Text2";
        String path3 = "/root/IdeaProjects/Parser/TextFiles/Text3";
        String path4 = "/root/IdeaProjects/Parser/TextFiles/Text4";
        String path5 = "/root/IdeaProjects/Parser/TextFiles/Text5";

        String[] paths = {path1, path2, path3, path4, path5};
        WordsStorage storage = new WordsStorage();
        ReportGenerator generator = new ReportGenerator();
        generator.start();

        RejectedExecutionHandler handler = new RejectedHandler();
        ThreadExecutor executor = new ThreadExecutor(paths);
        executor.startThreads(
                5,
                6,
                5,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(3),
                handler
        );

    }
}
