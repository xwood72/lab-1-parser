package com.company.Parser;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by root on 11.06.17.
 */
public class WordsStorage {

    private static volatile ConcurrentHashMap<String, Integer> map;
    private static AtomicInteger count;

    public WordsStorage() {
        map = new ConcurrentHashMap<>();
    }

    public static void addWord(String s) {
        count = new AtomicInteger(1);
        if (map.putIfAbsent(s, count.intValue()) != null){
            count.set(map.get(s));
            map.put(s, count.incrementAndGet());
        }
        String name = Thread.currentThread().getName();
        //System.out.printf("Word \"%s\" was added %d times\n", s, count.intValue());
        //System.out.printf("%s add word \" %s \" to storage\n",name, s);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Map.Entry<String,Integer>> getElements(){
        return new ArrayList<>(map.entrySet());
    }
}
