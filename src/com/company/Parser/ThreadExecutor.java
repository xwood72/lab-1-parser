package com.company.Parser;

import com.sun.xml.internal.ws.api.pipe.FiberContextSwitchInterceptor;

import java.util.concurrent.*;

/**
 * Created by root on 11.06.17.
 */
public class ThreadExecutor {
    private String[] paths;

    public ThreadExecutor(String[] paths) {
        this.paths = paths;
    }


    public void startThreads(int corePoolSize, int maxPoolSize, int keepAliveTime,
                             TimeUnit timeUnit,
                             BlockingQueue<Runnable> queue,
                             RejectedExecutionHandler handler){

        ThreadPoolExecutor service = new ThreadPoolExecutor(
                corePoolSize, //
                maxPoolSize, //
                keepAliveTime, //время на работу потоков в пуле
                timeUnit, // параметр указывающий размерность keepAliveTime
                queue, // размер очереди
                Executors.defaultThreadFactory(),
                handler
        );

        for (String path : paths) {
            Worker worker = new Worker(path);
            service.execute(worker);
        }

        service.shutdown();
        while (!service.isTerminated()){}
    }
}
