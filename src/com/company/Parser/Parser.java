package com.company.Parser;

/**
 * Created by root on 09.06.17.
 */
public class Parser {

    private static final char[] punctChars = {'!','?','.',',',':',';'};
    private static final char[] latinChars = {'a', 'b', 'c', 'd', 'e', 'f', 'j',
                                              'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                              'o', 'p', 'q', 'r', 's', 't', 'w',
                                              'v', 'x', 'y', 's'};
    public Parser() {}

    /**
     * Check every word in line for correct form and print it
     * @param line which you want to parse
     * @see Parser:isCyrillicWord
     */
    public void parseLine(String line){
        String[] words = line.split(" ");
        for (String word : words) {
            if (isCyrillicWord(word) > 0) {
                System.out.println(word);
            }
        }
    }

    /**
     * @param word which will be checked
     * @return -1 if word contain wrong (not cyrillic, digest or punctuation) symbol ,
     *         0 if word consist of cyrillic or punctuation symbols and contains even one digest
     *         1 if word consist of cyrillic or punctuation symbols
     */
    public static int isCyrillicWord(String word){
        if (word != null){
            char[] w = word.toCharArray();
            for (char aW : w) {
                if ((!Character.UnicodeBlock.CYRILLIC.equals(  // символ не русский и не знак препинания
                        Character.UnicodeBlock.of(aW)))
                        && !charIsPunctChar(aW)) {
                    for (char c: latinChars) {
                        if (aW == c){
                            return -1;
                        }
                    }
                    return 0;
                }
            }
        } else {
            return 0;
        }
        return 1;
    }

    /**
     * @param c char, which you want to check
     * @return true if character is punctuation symbol
     *         false if another
     */
    public static boolean charIsPunctChar(char c){
        for (char punctChar : punctChars) {
            if (c == punctChar) {
                return true;
            }
        }
        return false;
    }

}
