package com.company.Parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by root on 10.06.17.
 */
public class Worker extends Thread {

    private String path;

    public Worker(String path) {
        this.path = path;
    }

    @Override
    public void run() {
        try {
            try(BufferedReader br = new BufferedReader(new FileReader(path))){
                String line;
                int i = 0;
                while (((line = br.readLine()) != null) && !Main.isStop){
                    i++;
                    String[] words = line.split(" ");
                    for (String word : words) {
                        if (word.length() == 0){
                            continue;
                        }
                        switch (Parser.isCyrillicWord(word)){
                            case 1:
                                WordsStorage.addWord(word);
                                break;
                            case -1:
                                System.out.printf("Error was occurred at line %d in %s file.\n" +
                                        "Bad word \" %s \".\n", i, path, word);
                                Main.isStop = true;
                                break;
                        }
                    }
                }
                //System.out.println("Thread " + Thread.currentThread().getName()
                //        + " has stopped.");
            }
        } catch (IOException e) {
            System.out.printf("Sorry. IOException was occurred trying read the file from %s\n", path);
            e.printStackTrace();
        }
    }
}
