package com.company.Parser;
import de.vandermeer.asciitable.AsciiTable;

import java.io.IOException;
import java.util.*;

/**
 * Created by root on 11.06.17.
 */
public class ReportGenerator extends Thread {

    final String ANSI_CLS = "\"\033c";

    private void clearScreen(){
        System.out.print(ANSI_CLS);
        System.out.flush();
    }

    private String createTable(){
        Map.Entry<String, Integer> entry;
        ArrayList<Map.Entry<String, Integer>> arr = WordsStorage.getElements();
        if(arr.size() == 0) return "";
        Collections.reverse(arr);
        AsciiTable at = new AsciiTable();
        at.setPaddingTop(20);
        at.addRule();

        for(Iterator<Map.Entry<String, Integer>> i = arr.listIterator();
             i.hasNext(); ) {
            entry = i.next();
            //System.out.printf("Key %s --> %d times\n", entry.getKey(), entry.getValue());
            String col1 = entry.getKey();
            String col2 = (entry.getValue()).toString();
            at.addRow(col1, col2);
            at.addRule();
        }
        String rend = at.render();
        return rend;
    }

    @Override
    public void run() {
        while(!Main.isStop){
            clearScreen();

            //reverse list and print entries
            System.out.println(createTable());

            try {
                sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
